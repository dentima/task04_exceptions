package com.dentima;

import com.dentima.view.MyView;

public class Application {
  public static void main(String[] args) {
    new MyView().show();
  }
}
