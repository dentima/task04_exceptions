package com.dentima.util;

public class AutoCloseableCustomException extends Exception {
  public AutoCloseableCustomException(String message) {
    super(message);
  }
}
