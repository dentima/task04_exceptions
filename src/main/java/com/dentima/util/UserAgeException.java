package com.dentima.util;

public class UserAgeException extends RuntimeException {
  public UserAgeException(String message) {
    super(message);
  }
}
