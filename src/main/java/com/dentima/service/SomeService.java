package com.dentima.service;

import com.dentima.util.AutoCloseableCustomException;

public class SomeService implements AutoCloseable{
  public void printSomeAbracadabra (){
    System.out.println("Test AutoCloseable ");
  }

  @Override
  public void close() throws Exception {
    System.out.println("Close User Service class");
    throw new AutoCloseableCustomException("exception in AutoCloseable");
  }
}
