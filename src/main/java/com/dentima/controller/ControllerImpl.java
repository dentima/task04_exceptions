package com.dentima.controller;

import com.dentima.model.User;
import com.dentima.service.SomeService;
import com.dentima.util.UserAgeException;

public class ControllerImpl implements Controller {

  @Override
  public User createUser(String name, int age) {
    User user = new User();
    try {
      user.setUserName(name);
      user.setAge(age);
    } catch (UserAgeException ex){
      ex.printStackTrace();
    }
    return user;
  }

  @Override
  public void printAutoclouse() {

    try (SomeService service = new SomeService()){
      service.printSomeAbracadabra();
    } catch (Exception e){
      System.out.println("it's block catch");
    }
  }
}

